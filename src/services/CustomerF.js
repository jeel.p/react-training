let persons = [{ id: 1, name: 'jeel', email: 'jeel@gmail.com', phone: 9797797, address: 'ahmedabad' }]
function saveToStorage() {
    localStorage.setItem("_persons", JSON.stringify(persons));
}

// saveToStorage();

function getFromStorage() {
    var data = localStorage.getItem("_persons");
    if (data != null) {
        persons = JSON.parse(localStorage.getItem("_persons"));
    }
}
getFromStorage()
// let persons = JSON.parse(localStorage.getItem("_persons"))
export var getPerson = () => (persons)
export var addPersons = (person) => {
    persons.push(person)
    saveToStorage();
}
export var deletePersons = (id) => {
    let temp = persons.filter((person) => (person.id !== id));
    // console.log("temp: ", temp)      

    persons = [...temp]
    saveToStorage();
    // localStorage.setItem("_persons",JSON.stringify(persons))
    // console.log(persons)
}
export var editPersons = (newPerson) => {
    // console.log(newPerson)
    // console.log(newPerson.id)
    for (let i = 0; i < persons.length; i++) {
        // let person = {};
        if (persons[i].id === newPerson.id) {
            persons[i].id = newPerson.id;
            persons[i].name = newPerson.name;
            persons[i].email = newPerson.email;
            persons[i].phone = newPerson.phone;
            persons[i].address = newPerson.address;
        }
    }
    // localStorage.setItem("_persons",JSON.stringify(persons))
    saveToStorage();
}
export var copyPersons = (id) => {
    let person = {};
    person.id = Date.now();
    let temp = persons.filter(person => (person.id === id))
    person.name = temp[0].name;
    person.email = temp[0].email;
    person.phone = temp[0].phone;
    person.address = temp[0].address
    persons.push(person)
    // localStorage.setItem("_persons",JSON.stringify(persons))
    saveToStorage();
}