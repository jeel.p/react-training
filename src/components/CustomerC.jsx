import React, { Component } from 'react';
class CustomerC extends Component {
    state = {
        persons: [
            { id: 1, name: "jeel", email: 'jeel@gmail.com', phone: "9898989", address: "jejeijiijeif" }
        ],
        id: "",
        name: "",
        email: "",
        phone: "",
        address: "",
        editBtn: "Add"
    }


    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    addPerson = (e) => {
        e.preventDefault();
        if (this.state.editBtn === "Add") {
            const newPerson = {
                id: Date.now(),
                name: this.state.name,
                email: this.state.email,
                phone: this.state.phone,
                address: this.state.address
            }
            this.setState(prevState => ({
                persons: prevState.persons.concat(newPerson),
                id:"",
                name: "",
                email: "",
                phone: "",
                address: ""
            }))
        }

        else {
            let temp = this.state.persons.filter((person) => (person.id === this.state.id));
            temp=[]
            for (var i = 0; i < this.state.persons.length; i++) {
                let person={};
                if (this.state.persons[i].id === this.state.id) {
                    person.id=this.state.id;
                    person.name=this.state.name;
                    person.email=this.state.email;
                    person.phone=this.state.phone;
                    person.address=this.state.address;
                    temp.push(person)
                }
                else{
                    person.id=this.state.persons[i].id;
                    person.name=this.state.persons[i].name;
                    person.email=this.state.persons[i].email;
                    person.phone=this.state.persons[i].phone;
                    person.address=this.state.persons[i].address;
                    temp.push(person)
                }
            }
            this.setState({
                persons:temp,
                id:"",
                name: "",
                email: "",
                phone: "",
                address: "",
                editBtn:"Add"
            })

        }
    }
    // showPerson = () => {
    //     console.log("in showPerson")
    //     return (
    //         <ul>
    //             {
    //                 this.state.persons.map((person) => (
    //                     <li key={person.id}>Name: {person.name} Email: {person.email}</li>
    //                 )
    //                 )
    //             }
    //         </ul>
    //     )
    // }


    deletePerson = (id) => {
        console.log("Deleted id: " + id)
        let temp = this.state.persons.filter((person) => (person.id !== id));
        this.setState({ persons: temp });
    }

    editPerson = (id) => {
        console.log("Edited id: " + id)
        let temp = this.state.persons.filter((person) => (person.id === id));
        // console.log(temp[0].name)
        this.setState({
            id: temp[0].id,
            name: temp[0].name,
            email: temp[0].email,
            phone: temp[0].phone,
            address: temp[0].address,
            editBtn: "Update"
        })
    }

    showPerson = () => {
        // console.log("in showPerson")
        return (
            <tbody>

                {
                    this.state.persons.map((person) => (
                        <tr key={person.id}>
                            <td>{person.id}</td>
                            <td>{person.name}</td>
                            <td>{person.email}</td>
                            <td>{person.phone}</td>
                            <td>{person.address}</td>
                            <td><button onClick={() => (this.editPerson(person.id))}>Edit</button></td>
                            <td><button onClick={() => (this.deletePerson(person.id))}>Delete</button></td>
                        </tr>
                    )
                    )
                }
            </tbody>
        )
    }


    render() {
        return (
            <div>
                <form>
                    Name: <input type="text" required id="name" name="name" onChange={this.handleChange} value={this.state.name}></input><br /><br />
                    Email: <input type="email" required id="email" name="email" onChange={this.handleChange} value={this.state.email}></input><br /><br />
                    Phone: <input type="text" required id="phone" name="phone" onChange={this.handleChange} value={this.state.phone}></input><br /><br />
                    Address: <input type="text" required id="address" name="address" onChange={this.handleChange} value={this.state.address}></input><br /><br />
                    <input type="submit" onClick={this.addPerson} value={this.state.editBtn}></input>
                </form>
                <br /><br />
                {/* <br /><br /> */}
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>email</th>
                            <th>phone</th>
                            <th>address</th>
                            <th>edit</th>
                            <th>delete</th>
                            <th>copy</th>
                        </tr>
                    </thead>
                    {this.showPerson()}
                </table>
                {/* {this.showPerson()} */}
            </div>
        );
    }
}
export default CustomerC;