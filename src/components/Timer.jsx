import React, { Component, useState, useEffect } from "react";

export class TimerClass extends React.Component {
  componentDidMount() {
    console.log(">> componentDidMount");
    // this.doStart();
  }
  //value it is carry
  state = {
    count: 0,
  }; //initial value 0
  incrementCount = () => {
    this.setState(({ count }) => ({ count: count + 1 }));
  };
  doStart = () => {
    this.interval = setInterval(this.incrementCount, 1000);
  };
  doStop = () => {
    // Assignment
    clearInterval(this.interval);
  };

  doReset = () => {
    console.log(">> doRest ");
    this.setState(() => ({ count: 0 }));
  };
  render() {
    // this return html
    //console.log(">> render");
    return (
      <div>
        <h5>Class Timer</h5>
        Count State: {this.state.count} <br />
        <br />
        <button onClick={this.doStart}> Start</button>
        &nbsp;&nbsp;
        <button onClick={this.doReset}> Reset to 0</button>
        &nbsp;&nbsp;
        <button onClick={this.doStop}> Stop</button>
        <br />
        <br />
      </div>
    );
  }
}

export function TimerFunc() {
  const [count, setCount] = useState(0);
  const [intervalRef, setIntervalRef] = useState(0);
  useEffect(() => {
    // doStart();
  }, []);

  const incrementCount = () => {
    setCount((prevVal) => prevVal + 1);
  };

  const doStart = () => {
    //put interval in state / useState
    let temp = setInterval(incrementCount, 1000);
    setIntervalRef(temp);
  };
  const doStop = () => {
    // Assignment to stop Timer
    clearInterval(intervalRef);
  };

  const doReset = () => {
    console.log(">> doRest ");
    setCount(0);
  };
  return (
    <div>
      <h5>Function Timer</h5>
      Count : {count} <br />
      <br />
      <button onClick={doStart}> Start</button>
      &nbsp;&nbsp;
      <button onClick={doReset}> Reset to 0</button>
      &nbsp;&nbsp;
      <button onClick={doStop}> Stop</button>
      <br />
      <br />
    </div>
  );
}
