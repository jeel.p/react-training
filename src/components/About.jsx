import React, { useEffect, useRef, useState } from 'react';
function About() {
    const count = useRef(0)
    const [interval, setInter] = useState(0)
    useEffect(() => {       // will be rendered on when any change happened in state element
        // count.current = count.current + 1;
        // start();
        count.current = count.current + 1
        console.log("in use effect")
        console.log(count.current)
    })
    const start = () => {
        const interva = setInterval(() => {
            console.log("count: ", count.current)
            count.current = count.current + 1;
        }, 1000)
        setInter(interva)
        // setInput(Date.now())
        console.log("clicked start button")
    }
    const stop = () => {
        clearInterval(interval)
        setInter(0)
        console.log("clicked stop button")
    }


    return (
        <div>
            <h2>About</h2>
            <h3>count: {count.current}</h3>
            <button onClick={start}>start</button>
            <button onClick={stop}>stop</button>
        </div>
    );
}
export default About;
