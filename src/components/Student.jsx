import React, { Component, useState } from 'react';

function Student() {
    const [students, setStudents] = useState([{ id: 1, name: "jeel", email: 'jeel@gmail.com', city: "Ahmedabad", subject: "React Js", teacher:"xyz", school:"abc" }]);

    const [id, setId] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [city, setCity] = useState("");
    const [subject, setSubject] = useState("");
    const [teacher, setTeacher] = useState("");
    const [school, setSchool] = useState("");
    const [editBtn, setEditBtn] = useState("Add");

    const addStudent = (e) => {
        e.preventDefault();
        if (editBtn === "Add") {
            const newStudent = {
                id: Date.now(),
                name: name,
                email: email,
                city: city,
                subject: subject,
                teacher: teacher,
                school: school,
            }
            setId("");
            setName("");
            setEmail("");
            setCity("");
            setSubject("");
            setTeacher("");
            setSchool("");

            setStudents(students.concat(newStudent))
            console.log("students", students);

        }

        else {
            let temp = students.filter((student) => (student.id === id));
            temp = []
            for (var i = 0; i < students.length; i++) {
                let student = {};
                if (students[i].id === id) {
                    student.id = id;
                    student.name = name;
                    student.email = email;
                    student.city = city;
                    student.subject = subject;
                    student.teacher = teacher;
                    student.school = school;
                    temp.push(student)
                }
                else {
                    student.id = students[i].id;
                    student.name = students[i].name;
                    student.email = students[i].email;
                    student.city = students[i].city;
                    student.subject = students[i].subject;
                    student.teacher = students[i].teacher;
                    student.school = students[i].school;
                    temp.push(student)
                }
            }

            setStudents(temp)
            setId("");
            setName("");
            setEmail("");
            setCity("");
            setSubject("");
            setTeacher("");
            setSchool("");
            setEditBtn("Add")

        }
    }



    const deleteStudent = (id) => {
        console.log("Deleted id: " + id)
        let temp = students.filter((student) => (student.id !== id));
        // this.setState({ students: temp });
        setStudents(temp)
    }

    const editStudent = (id) => {
        console.log("Edited id: " + id)
        let temp = students.filter((student) => (student.id === id));
        setId(temp[0].id);
        setName(temp[0].name);
        setEmail(temp[0].email);
        setCity(temp[0].city);
        setSubject(temp[0].subject);
        setTeacher(temp[0].teacher);
        setSchool(temp[0].school);
        setEditBtn("Update")
    }

    const copyStudent=(id)=>{
        console.log("Copied id: "+id);
        let temp=[]
        temp = students.filter((student) => (student.id === id));
        const newStudent = {
            id: Date.now(),
            name: temp[0].name,
            email: temp[0].email,
            phone: temp[0].phone,
            city: temp[0].city,
            subject: temp[0].subject,
            teacher: temp[0].teacher,
            school: temp[0].school,
        }
        setStudents(students.concat(newStudent))
    }

    const showStudents = () => {
        return (
            <tbody>
                {
                    students.map((student) => (
                        <tr key={student.id}>
                            <td>{student.id}</td>
                            <td>{student.name}</td>
                            <td>{student.email}</td>
                            <td>{student.city}</td>
                            <td>{student.subject}</td>
                            <td>{student.teacher}</td>
                            <td>{student.school}</td>
                            <td><button onClick={() => (editStudent(student.id))}>Edit</button></td>
                            <td><button onClick={() => (deleteStudent(student.id))}>Delete</button></td>
                            <td><button onClick={() => (copyStudent(student.id))}>Copy</button></td>
                        </tr>
                    )
                    )
                }
            </tbody>
        )
    }

    return (
        <div>
            <form>
                Name: <input type="text" required id="name" name="name" onChange={(e) => (setName(e.target.value))} value={name}></input><br /><br />
                Email: <input type="email" required id="email" name="email" onChange={(e) => (setEmail(e.target.value))} value={email}></input><br /><br />
                City: <input type="text" required id="city" name="city" onChange={(e) => (setCity(e.target.value))} value={city}></input><br /><br />
                Subject: <input type="text" required id="subject" name="subject" onChange={(e) => (setSubject(e.target.value))} value={subject}></input><br /><br />
                teacher: <input type="text" required id="teacher" name="teacher" onChange={(e) => (setTeacher(e.target.value))} value={teacher}></input><br /><br />
                school: <input type="text" required id="school" name="school" onChange={(e) => (setSchool(e.target.value))} value={school}></input><br /><br />
                <input type="submit" onClick={addStudent} value={editBtn}></input>
            </form>
            <br /><br />
            <table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>email</th>
                        <th>city</th>
                        <th>subject</th>
                        <th>teacher</th>
                        <th>school</th>
                        <th>edit</th>
                        <th>delete</th>
                        <th>copy</th>
                    </tr>
                </thead>
                {showStudents()}
            </table>
        </div>
    );
}
export default Student;