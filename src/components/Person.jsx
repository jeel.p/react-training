import React, { Component, useEffect, useState } from "react";
export class PersonC extends Component {
  state = { count: 0 };
  increment = () => {
    console.log("PersonC>>>");
    console.log("Count is " + this.state.count);
    this.setState({ count: this.state.count + 1 });
  };
  render() {
    return (
      <div>
        <h3>
          class {this.state.count}Person class name={this.props.name} email=
          {this.props.email}
        </h3>
        <button onClick={this.increment}>Increment</button>
      </div>
    );
  }
}

export function PersonF({ name, email }) {
  // const [count, setCount] = useState(0);
  const [cnt, setCnt] = useState(0);
  useEffect(() => {
    console.log("Mounted");
  }, [cnt]);
  const increment = () => {
    console.log("PersonF>>>");
    // setCnt(count + 1);
    setCnt(cnt + 1); // This will work
    console.log("Count is: ", cnt);
    setCnt(cnt + 2);  // This will not called
  };
  return (
    <div>
      <h3>
        Person function name={name} email={email}
      </h3>
      <button onClick={increment}>Increment</button>
    </div>
  );
}

// export default PersonC;
