import React, { useState } from 'react';
import { getPerson, addPersons, deletePersons, editPersons, copyPersons } from "../services/CustomerF"

function CustomerF() {
    // let personContext=createContext();
    const [persons, setPersons] = useState(getPerson());

    const [id, setId] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [editBtn, setEditBtn] = useState("Add");
    const [cnt, setCnt] = useState(0)

    const addPerson = (e) => {
        e.preventDefault();
        if (editBtn === "Add") {
            const newPerson = {
                id: Date.now(),
                name: name,
                email: email,
                phone: phone,
                address: address
            }
            addPersons(newPerson)
            // setPersons(getPerson())
            // console.log("persons", persons);

        }

        else {
            // let temp = persons.filter((person) => (person.id === id));
            let person = {};
            person.id = id;
            person.name = name;
            person.email = email;
            person.phone = phone;
            person.address = address;
            editPersons(person);
            // temp = []
            // for (var i = 0; i < persons.length; i++) {
            //     let person = {};
            //     if (persons[i].id === id) {
            //         person.id = id;
            //         person.name = name;
            //         person.email = email;
            //         person.phone = phone;
            //         person.address = address;
            //         temp.push(person)
            //     }
            //     else {
            //         person.id = persons[i].id;
            //         person.name = persons[i].name;
            //         person.email = persons[i].email;
            //         person.phone = persons[i].phone;
            //         person.address = persons[i].address;
            //         temp.push(person)
            //     }
            // }

            // setPersons(getPerson())
        }
        setCnt(cnt + 1)
        setId("");
        setName("");
        setEmail("");
        setPhone("");
        setAddress("");
        setEditBtn("Add");
        setPersons(getPerson())
    }



    const deletePerson = (id) => {
        console.log("Deleted id: " + id)
        deletePersons(id)
        setPersons(getPerson())
        setCnt(cnt + 1)
        // let temp = persons.filter((person) => (person.id !== id));
        // this.setState({ persons: temp });
        // setPersons(getPerson())
    }

    const editPerson = (id) => {
        console.log("Edited id: " + id)
        let temp = getPerson().filter((person) => (person.id === id));
        setId(temp[0].id);
        setName(temp[0].name);
        setEmail(temp[0].email);
        setPhone(temp[0].phone);
        setAddress(temp[0].address);
        setEditBtn("Update")
    }

    const copyPerson = (id) => {

        console.log("Copied id: " + id);
        // let temp = []
        // temp = persons.filter((person) => (person.id === id));
        // const newPerson = {
        //     id: Date.now(),
        //     name: temp[0].name,
        //     email: temp[0].email,
        //     phone: temp[0].phone,
        //     address: temp[0].address
        // }
        // setPersons(persons.concat(newPerson));
        copyPersons(id);
        setPersons(getPerson())
        setCnt(cnt + 1)
        // setPersons(await getPerson());
    }

    let ShowPerson = () => {
        return (
            <tbody>
                {
                    persons.map((person) => (
                        <tr key={person.id}>
                            <td>{person.id}</td>
                            <td>{person.name}</td>
                            <td>{person.email}</td>
                            <td>{person.phone}</td>
                            <td>{person.address}</td>
                            <td><button className='btn btn-outline-secondary' onClick={() => (editPerson(person.id))}>Edit</button></td>
                            <td><button className='btn btn-outline-danger' onClick={() => (deletePerson(person.id))}>Delete</button></td>
                            <td><button className='btn btn-outline-success' onClick={() => (copyPerson(person.id))}>Copy</button></td>
                        </tr>
                    )
                    )
                }
            </tbody>
        )
    }

    return (
        <div>

            <div className="col-md-4 col-md-offset-4 center">
                <form>
                    <div className="mb-3">
                        <label for="name" className="form-label">Name</label>
                        <input type="text" className="form-control" name="name" id="name" onChange={(e) => (setName(e.target.value))} value={name} />
                    </div>
                    <div className="mb-3">
                        <label for="email" className="form-label">Email</label>
                        <input type="email" className="form-control" name="email" id="email" onChange={(e) => (setEmail(e.target.value))} value={email} />
                    </div>
                    <div className="mb-3">
                        <label for="phone" className="form-label">Phone</label>
                        <input type="number" className="form-control" name="phone" id="phone" onChange={(e) => (setPhone(e.target.value))} value={phone} />
                    </div>
                    <div className="mb-3">
                        <label for="address" className="form-label">Address</label>
                        <input type="text" className="form-control" name="address" id="address" onChange={(e) => (setAddress(e.target.value))} value={address} />
                    </div>
                    <input type="submit" className='btn btn-outline-primary' onClick={addPerson} value={editBtn}></input>

                </form>
            </div>

            <br /><br />

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>email</th>
                        <th>phone</th>
                        <th>address</th>
                        <th>edit</th>
                        <th>delete</th>
                        <th>copy</th>
                    </tr>
                </thead>
                {/* {showPerson()} */}
                <ShowPerson />
                {/* <ShowPerson/> */}
            </table>

        </div>
    );
}
export default CustomerF;