// import logo from './logo.svg';
import "./App.css";
import React from "react";
// import { PersonC, PersonF } from "./components/Person";
// import { TimerClass, TimerFunc } from "./components/Timer";
// import CustomerC from "./components/CustomerC";
// import Student from "./components/Student"
import CustomerF from "./components/CustomerF"
import Home from "./components/Home";
import About from "./components/About"
import PageNotFound from "./components/PageNotFound"
import Login from "./components/Login"
import { BrowserRouter, Routes, Route, Navigate, NavLink,Outlet } from 'react-router-dom';

// const heading = <h1>Heading</h1>;
// const doLogin = () => {
//   alert("Do login");
// };

const App = function () {
  return (
    <BrowserRouter>
      <div className="App">
        {/* {heading} */}
        {/* <PersonF name="jeel" email="jeel@gmail.com" />
      <PersonC name="jeel" email="jeel@gmail.com" /> */}
        {/* <TimerClass />
      <TimerFunc /> */}
        {/* <button onClick={doLogin}>Login</button> */}

        {/* <hr></hr>
      <h2>Customer Class</h2>
      <hr></hr>
      <CustomerC />
      <br />
      <br /> */}

        {/* <hr></hr>
      <h2>Customer Function</h2>
      <hr></hr>
      <CustomerF /> */}

        {/* <hr></hr>
      <h2>Student</h2>
      <hr></hr>
      <Student /> */}
        {/* <ul>
          <li>
            <Link to="/home">Home</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/customer">Customer</Link>
          </li>
        </ul> */}


        <nav className="navbar navbar-expand-lg bg-light">
          <div className="container-fluid">
            <NavLink className="navbar-brand" to="/">Navbar</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/home">Home</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/login">Login</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customer">Customer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/about">About</NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>




        <Routes>
          <Route exact path="/" element={<Navigate to={{ pathname: "/login" }} />} />
          {/* <Redirect exact from="/" to="/login" /> */}
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/home" element={<Home />} />
          <Route exact path="/about" element={<About />} />
          <Route exact path="/customer" element={<CustomerF />} />
          {/* <Route exact path="/customer/add" element={<AddCustomer/>}/>
           <Route exact path="/customer/edit/:recordId" element={<AddCustomer/>}/> */}
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </div>

    </BrowserRouter>
  );
};

export default App;
